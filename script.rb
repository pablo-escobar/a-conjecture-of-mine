# This script is a simple test for the following conjecture:

# Let S: N -> N be the sum of the digits of a positive integer.
# For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an interger.

def sum_digits i
    part = i.abs()
    sum = 0

    while part > 0
        d, r = part.divmod 10
        sum += r
        part = d
    end

    sum
end

def get_sums max
    (0 .. 2 * max).map {|i| sum_digits i}
end

def counterexempl? max
    sums_cache = get_sums max

    for a in 0..max
        for b in a..max
            diff = sums_cache[a + b] - sums_cache[a] - sums_cache[b]
            return true if diff % 9 != 0
        end
    end

    false
end

