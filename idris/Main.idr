-- The following program is a simple test for the following conjecture:

-- Let S: N -> N be the sum of the digits of a positive integer.
-- For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an integer.

module Main where

import Numeric (readDec)
import Numeric.Natural
import System.Environment
import System.Exit
import Control.Monad (foldM)
import Data.Vector (Vector, unsafeIndex, generate)

main : IO Int
main = do
    args <- getArgs
        
    case readDec <$> head' args of
        Just [(max, "")] =>
            if counterexempl max then exitFailure else exitSuccess

        _ => exitInvalidInput
    
    where head' [] = Nothing
          head' xs = Just (head xs)

-- Calculates the sum of the digits of `n`.
sumDigits : Int -> Int
sumDigits = sumDigitsTail 0
    where sumDigitsTail acc n
              | n == 0 = acc
              | otherwise = sumDigitsTail (acc + (n `mod` 10)) (n `div` 10)

-- Returns `True` if the if the conjecture holds for pair.
-- Otherwise returns `False`.
test' : Vector Int -> (Int, Int) -> Bool
test' sums (a, b) = diff `mod` 9 == 0
    where diff = sums ! (a + b) - sums ! a - sums ! b
          (!) = unsafeIndex

-- Checks if there is any counterexample in
-- [(a, b) | a <- [0..max], b <- [a..max]].
--
-- Returns `True` if a counter example was found.
-- Otherwise returns `False`.
counterexempl : Int -> Bool
counterexempl max =
    all (test' sums) [(a, b) | a <- [0..max], b <- [a..max]]
    where sums = generate (2 * max + 1) sumDigits

exitInvalidInput : IO Int
exitInvalidInput = exitWith $ ExitFailure 2

