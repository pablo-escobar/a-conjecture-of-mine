;; The following program is a simple test for the following conjecture:

;; Let S: N -> N be the sum of the digits of a positive integer.
;; For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an integer.

(module
    ;; Check if there is any counterexample 
    ;; in {(x, y) | 0 <= x <= a, 0 <= y <= x}
    (func (export "counterexempl") (param $a i32) (result i32)
        (local $b i32)

        ;; while a != 0
        (block $break_0             
               (loop $while_0
                     (br_if $break_0 (i32.eqz (local.get $a)))

                     ;; b = a
                     (local.set $b (local.get $a))

                     ;; while b != 0 
                     (block $break_1                                         
                            (loop $while_1
                                  (br_if $break_1 (i32.eqz (local.get $b)))
                                  (if ;; if test(a, b)
                                    (call $test (local.get $a) (local.get $b))
                                    
                                    ;; return 1                  
                                    (block (i32.const 1) (return))                              
                                    
                                    (block ;; else

                                      ;; b -= 1
                                      (local.set $b 
                                                 (i32.sub 
                                                   (local.get $b) 
                                                   (i32.const 1)
                                                 )
                                      ) 
                                      (br $while_1) ;; continue
                                    )
                                  )
                            )
                     )

                     ;; a -= 1
                     (local.set $a (i32.sub (local.get $a) (i32.const 1))) 
                     (br $while_0) ;; continue
               )
        )

        (i32.const 0) ;; return 0
    )

    ;; Calculates the sums of the digits of a non-negative integer.
    (func $sum_digits (param $n i32) (result i32)
        (local $sum i32)
        (local.set $sum (i32.const 0))

        (block $break
               (loop $while    
                     ;; Break if n == 0i32
                     (br_if $break (i32.eqz (local.get $n)))
               
                     ;; sum += n % 10
                     (local.set $sum 
                                (i32.add
                                  (local.get $sum)
                                  (i32.rem_u (local.get $n) (i32.const 10))
                                )
                     )
                     
                     ;; n /= 10
                     (local.set $n (i32.div_u (local.get $n) (i32.const 10)))

                     ;; Go to `while`
                     (br $while)
               )
        )

        (local.get $sum) ;; return sum
    )

    ;; Checks if (sum_digits(a + b) - sum_digits(a) - sum_digits(b)) % 9 != 0
    (func $test (export "test") (param $a i32) (param $b i32) (result i32) 
          (i32.ne
            (i32.const 0) 
            (i32.rem_s
              (i32.sub 
                (call $sum_digits (i32.add (local.get $a) (local.get $b)))
                (i32.add
                  (call $sum_digits (local.get $a))
                  (call $sum_digits (local.get $b))
                )
              )
              (i32.const 9)
            )
          )
    )
)


