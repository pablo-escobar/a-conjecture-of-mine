⍝ This script is a simple test for the following conjecture:

⍝ Let S: N -> N be the sum of the digits of a positive integer.
⍝ For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an interger.

⍝ Computes the sum of the digits of a positive integer N
∇ SUM←SUMDIGITS N
  SUM←+/10|⌊N÷10⋆(⍳⌊1+10⍟N)-1
∇

⍝ Given a positive integer A, returns 1 if the conjecture holds for all pairs
⍝ (A, B) with B = 1, ..., A and 0 otherwise
∇ RESULT←TEST A ; BS ; SUMSBS ; SUMSAPLUSBS
  BS←⍳A
  SUMSBS←SUMDIGITS¨BS
  SUMSAPLUSBS←SUMDIGITS¨BS+A
  RESULT←∧/0=9|SUMSAPLUSBS-SUMSBS+A
∇

⍝ Checks if the conjecture holds for all pairs (A, B) with A, B = 1, ..., MAX
∇ RESULT←CONJECTURE MAX
  RESULT←∧/TEST¨⍳MAX
∇

