package main

// This program is a simple test for the following conjecture:

// Let S: N -> N be the sum of the digits of a positive integer.
// For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an interger.

import (
    "os"
    "strconv"
)

type iter struct {
    Start uint
    End   uint
    Step  uint
}

const success = 0
const fail = 1
const invalidInput = 2

func main() {
    if len(os.Args) > 1 {
        var nGoRoutines uint = 1

        if len(os.Args) > 2 {
            arg, err := strconv.ParseUint(os.Args[2], 10, 64)
            if err == nil && arg >= 1 {
                nGoRoutines = uint(arg)
            }
        }

        max, err := strconv.ParseUint(os.Args[1], 10, 64)
        if err == nil {
            if counterexempl(uint(max), nGoRoutines) {
                os.Exit(fail)
            } else {
                os.Exit(success)
            }
        } else {
            os.Exit(invalidInput)
        }
    } else {
        os.Exit(invalidInput)
    }

}

func counterexempl(max uint, nGoRoutines uint) bool {
    sums := getSums(max)

    if nGoRoutines > 1 {
        channels := make([](chan bool), nGoRoutines)

        // Compute the result of each sub-range
        for i := uint(0); i < nGoRoutines; i++ {
            channels[i] = make(chan bool)
            it := iter{ uint(i), max, nGoRoutines }

            go counterexemplRange(it, &sums, channels[i])
        }

        // Listen for the computation to finish
        for _, c := range channels {
            if msg := <-c; msg {
                return true
            }
        }
    } else {
        c := make(chan bool)
        it := iter{0, max, 1}

        go counterexemplRange(it, &sums, c)

        return <-c
    }

    return false
}

func counterexemplRange(it iter, sums *[]int, c chan bool) {
    for a := it.Start; a <= it.End; a += it.Step {
        for b := a; b <= it.End; b++ {
            diff := (*sums)[a+b] - (*sums)[a] - (*sums)[b]

            if diff%9 != 0 {
                c <- true
                close(c)

                return
            }
        }
    }

    c <- false
    close(c)
}

func getSums(max uint) []int {
    maxRange := 2 * max + 1
    sums := make([]int, maxRange)

    for i := range sums {
        sums[i] = sumDigits(uint(i))
    }

    return sums
}

func sumDigits(n uint) int {
    var sum uint

    for {
        if n <= 0 {
            return int(sum)
        }

        sum += n % 10
        n /= 10
    }
}
