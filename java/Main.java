// The following program is a simple test for the following conjecture:

// Let S: N -> N be the sum of the digits of a positive integer.
// For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an integer.

import java.util.InputMismatchException;

public class Main {
    private static final int SUCCESS = 0;
    private static final int FAIL = 1;
    private static final int INVALID_INPUT = 2;

    public static void main(String[] args) {
        try {
            int max = Integer.parseInt(args[0]);
            if (max <= 0) throw new IllegalArgumentException();

            if (counterexample(max)) {
                System.exit(FAIL);
            } else {
                System.exit(SUCCESS);
            }
        } catch (Exception error) {
            System.exit(INVALID_INPUT);
        }

    }

    private static Boolean counterexample(int max) {
        int[] sum = getSums(max);

        for (int a = 0; a <= max; a++)
            for (int b = a; b <= max; b++) {
                int diff = sum[a + b] - sum[a] - sum[b];

                if (diff % 9 != 0)
                    return true;
            }

        return false;
    }

    private static int[] getSums(int max) {
        int maxRange = 2 * max + 1;
        int[] sums = new int[maxRange];

        for (int i = 0; i < maxRange; i++)
            sums[i] = sumDigits(i);

        return sums;
    }

    /**
     * Calculates the sum of the digits of a positive integer.
     */
    private static int sumDigits(int n) {
        int num = n, sum = 0;

        while (num > 0) {
            sum += num % 10;
            num /= 10;
        }

        return sum;
    }
}

