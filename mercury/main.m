:- module main.
:- interface.
:- import_module io.
:- import_module int.
:- pred main(io::di, io::uo) is det.
:- implementation.

:- func sum_digits_acc(int, int) = int.
sum_digits_acc(N, Acc) = (if N = 0 then Acc else sum_digits_acc(N div 10, Acc + (N mod 10))).

:- func sum_digits(int) = int.
sum_digits(N) = sum_digits_acc(N, 0).

main(!IO) :-
  io.write_int(sum_digits(55), !IO),
  io.nl(!IO).
