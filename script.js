// This script is a simple test for the following conjecture:

// Let S: N -> N be the sum of the digits of a positive integer.
// For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an interger.

function sumDigits(n) {
    let parc = Math.abs(n), sum = 0;

    while (parc > 0) {
        sum += parc % 10;
        parc = Math.floor(parc / 10);
    }

    return sum;
}

function getSums(max) {
    return Array.from({length: 2 * max}, (_, i) => sumDigits(i));
}

export function counterexempl(max) {
    const sums = getSums(max);

    for (let a = 1; a <= max; a++)
        for (let b = a; b <= max; b++) {
            const diff = sums[a + b] - sums[a] - sums[b];
            if (diff % 9 !== 0) return true;
        }

    return false;
}

