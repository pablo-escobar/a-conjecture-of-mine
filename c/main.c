#include <stdlib.h>
#include <stdio.h>

#define SUCCESS 0
#define FAIL 1
#define INVALID_INPUT 2

// This program is a simple test for the following conjecture:

// Let S: N -> N be the sum of the digits of a positive integer.
// For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an integer.

int sum_digits(unsigned int n)
{
    int parc = n;
    int sum = 0;

    while (parc > 0)
    {
        sum += parc % 10;
        parc /= 10;
    }

    return sum;
}

// Searches for a counterexample in an specific range.
int counterexempl(int max, int *sums_cache)
{
    for (int a = 0; a <= max; a++)
        for (int b = a; b <= max; b++)
            if ((sums_cache[a + b] - (sums_cache[a] + sums_cache[b])) % 9 != 0)
                return FAIL;

    return SUCCESS;
}

int main(int argc, char *argv[])
{
    if (argc > 1)
    {
        unsigned int max = strtoul(argv[1], NULL, 10);
	      if (!max) return INVALID_INPUT;

        // Create the sums cache
        int *sums_cache = malloc(sizeof(int) * (2 * max + 1));
        for (int i = 0; i <= 2 * max + 1; i++)
            sums_cache[i] = sum_digits(i);

        return counterexempl(max, sums_cache);
    }
    else 
    {
        return INVALID_INPUT;
    }
}

