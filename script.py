# This script is a simple test for the following conjecture:

# Let S: N -> N be the sum of the digits of a positive integer.
# For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an interger.

def sum_digits(n: int) -> int:
    parc = abs(n)
    sum_d = 0

    while parc > 0:
        sum_d += parc % 10
        parc //= 10

    return sum_d

def get_sums(m: int) -> list:
    return [sum_digits(i) for i in range(2 * m)]

def counterexempl(m: int) -> bool:
    sums = get_sums(m)

    for a in range(m + 1):
        for b in range(a, m + 1):
            diff = sums[a + b] - sums[a] - sums[b]
            if diff % 9 != 0: return True

    return False

