package conjecture

import kotlin.math.absoluteValue
import kotlin.system.exitProcess
import kotlin.collections.HashMap

// The following program is a simple test for the following conjecture:

// Let S: N -> N be the sum of the digits of a positive integer.
// For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an integer.

val OK = 0
val FAIL = 1
val INVALID_INPUT = 2

fun main(args: Array<String>) {
    try {
        val max = args[0].toInt()
        if (max <= 0) throw IllegalArgumentException()

        val sumsCache = IntArray(2 * max + 1) { n -> sumDigits(n) }

        if (counterexample(max, sumsCache)) exitProcess(FAIL)
        else exitProcess(OK)

    } catch (_: Exception) {
        exitProcess(INVALID_INPUT)
    }
}

/**
 * Searches for a counterexample for the theorem in
 * `{(a, b) in N^2 | a <= max, b <= a}`.
 */
fun counterexample(max: Int, sumsCache: IntArray): Boolean {
    for (a in 0..max)
        for (b in a..max) {
            val diff = sumsCache[a + b] - sumsCache[a] - sumsCache[b]
            if (diff % 9 != 0) return true
        }

    return false
}

/**
 * Calculates the sum of the digits of a positive integer.
 */
fun sumDigits(n: Int): Int {
    var sum = 0
    var num = n

    while (num > 0) {
        sum += num % 10
        num /= 10
    }

    return sum
}

