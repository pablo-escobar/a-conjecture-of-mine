.PHONY: all c cpp elixir go haskell java kotlin ocaml rust wasm mercury

all: c cpp elixir go haskell java ocaml rust wasm

c:
	gcc -O2 -o bin/c ./c/main.c

cpp:
	g++ -O2 -o ./bin/cpp -std=c++11 ./c++/main.cpp

elixir:
	elixirc -o bin/elixir ./elixir/main.ex

go:
	cd ./go/ && go build && mv ./go ../bin/go

haskell:
	ghc --make -O -o ./bin/haskell ./haskell/Main.hs \
	  && rm ./haskell/*.o ./haskell/*.hi

java:
	javac ./java/Main.java
	jar cfe ./bin/java.jar Main ./java/Main.class && rm ./java/*.class

kotlin:
	kotlinc ./kotlin/main.kt -d ./bin/kotlin.jar

ocaml:
	ocamlfind                  \
		ocamlopt                 \
		-thread                  \
		-package threads         \
		-nodynlink               \
		-o ./bin/ocaml           \
		-linkpkg ./ocaml/main.ml \
	  && rm ./ocaml/*.cmi ./ocaml/*.cmx ./ocaml/*.o

rust:
	rustc -v -O -o ./bin/rust ./rust/main.rs

wasm:
	wat2wasm ./wasm/main.wat -o ./bin/wasm.wasm

mercury:
	cd mercury && mmc main.m -o bin/mercury
	rm mercury/*.o mercury/*.c

proof.pdf: proof.tex
	xelatex -halt-on-error proof.tex 
	ls ./*.* \
		| sed -E '/\.(aux|bak|bbl|blg|log|nav|out|xml|snm|toc)$$/!d' \
		| sed "s/\(.*\)/'\1'/" \
		| xargs -r rm

