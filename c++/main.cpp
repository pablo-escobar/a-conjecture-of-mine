#include <iostream>
#include <string>
#include <thread>
#include <map>
#include <vector>

#define SUCCESS 0
#define FAIL 1
#define INVALID_INPUT 2

inline int sum_digits(unsigned int n)
{
    unsigned int sum = 0;

    while (n != 0)
    {
        sum += (n % 10);
        n /= 10;
    }

    return sum;
}

bool counterexpl(unsigned int max, std::vector<int> sums_cache)
{
    for (auto a = 0; a <= max; a ++)
        for (auto b = a; b <= max; b++)
            if ((sums_cache[a + b] - sums_cache[a] - sums_cache[b]) % 9 != 0)
                return true;

    return false;
}

int main(int argc, char *argv[])
{
    if (argc < 2) return INVALID_INPUT;
    unsigned int max = std::stoul(argv[1]);

    std::vector<int> sums_cache;
    
    // Builds the sums cache
    for (int i = 0; i <= 2 * max; i++)
        sums_cache[i] = sum_digits(i);

    return counterexpl(max, sums_cache) ? FAIL : SUCCESS;
}

