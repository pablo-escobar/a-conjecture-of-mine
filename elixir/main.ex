# The following program is a simple test for the following conjecture:

# Let S: N -> N be the sum of the digits of a positive integer.
# For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an integer.

defmodule Conjecture do
  def main max, n_processes do
    if max < 0 or n_processes <= 0 do
      :invalid_input
    else
      parent_id = self()
      :ets.new(:sums_cache, [:set, :public, :named_table])

      # Spawn a new process for each starting index from 0 to `max`
      f = fn i -> 
        spawn fn -> counterexpl i, max, n_processes, parent_id end
      end

      Enum.map 0..(n_processes - 1), f
      listen n_processes
    end
  end

  # Listen for messages on all processes
  def listen 0 do :ok end

  def listen n do
    receive do
      :ok -> listen (n - 1)
      :fail -> :fail
    end
  end

  def counterexpl a, max, step, parent_id do
    cond do 
      iter a, a -> send parent_id, :fail
      a + step <= max ->
        counterexpl (a + step), max, step, parent_id
      true -> send parent_id, :ok
    end
  end

  def iter a, 0 do test a, 0 end

  def iter a, b do
    test(a, b) || iter(a, b - 1)
  end

  def test a, b do
    c = a + b
    sum_a = lookup a
    sum_b = lookup b
    sum_c = lookup c

    0 != rem(sum_c - sum_a - sum_b, 9)
  end

  def lookup n do
    case :ets.lookup(:sums_cache, n) do
      [{_, sum_n}] -> sum_n
      [] ->
        sum_n = sum_digits n
        :ets.insert(:sums_cache, {n, sum_n})
        sum_n
    end
  end

  def sum_digits n do sum_digits_tail n, 0 end

  def sum_digits_tail 0, acc do acc end

  def sum_digits_tail n, acc do
    r = rem n, 10
    d = div n, 10
    sum_digits_tail d, (acc + r)
  end 
end

