// The following program is a simple test for the following conjecture:

// Let S: N -> N be the sum of the digits of a positive integer.
// For all A and B in N, S(A + B) = S(A) + S(B) - 9k, where k is an integer.

use std::{env, process};

const SUCCESS: i32 = 0;
const FAIL: i32 = 1;
const INVALID_INPUT: i32 = 2;

macro_rules! parse_arg {
    ($argv: expr) => {{
        match $argv.next().and_then(|s| s.trim().parse().ok()) {
            Some(n) => n,
            None    => process::exit(INVALID_INPUT)
        }
    }}
}

fn main() {
    let mut args = env::args();
    args.next();

    let max = parse_arg!(args);

    if counterexempl(max) {
        process::exit(FAIL);
    } else {
        process::exit(SUCCESS);
    }
}

fn counterexempl(max: usize) -> bool {
    let sums_cache = get_sums(max);

    for a in 0..max {
        for b in a..max {
            if (sums_cache[a + b] - sums_cache[a] - sums_cache[b]) % 9 != 0 {
                return true;
            }
        }
    }

    false
}

fn sum_digits(n: usize) -> isize {
    let mut sum = 0;
    let mut part = n;

    while part != 0 {
        sum += part % 10;
        part /= 10;
    }

    sum as isize
}

fn get_sums(max: usize) -> Vec<isize> {
    let mut output = Vec::with_capacity(2 * max);

    for n in 0..max * 2 {
        output.push(sum_digits(n));
    }

    output
}

